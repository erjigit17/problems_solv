def tsezar(list, n):
    l = len(list)
    res = []
    if n > 0:
        for i in range(l):
            if n + i >= l-1:
                res.append(list[(n+i)-l])
            else:
                res.append(list[n + i])
        return res
    else:
        for i in range(l):
            if i + n < 0:
                res.append(list[l + n + i])
            else:
                res.append(list[i + n])
        return res


list = input('Enter list: ').split()
n = int(input('Enter shift: '))
print(tsezar(list, n))

