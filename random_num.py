def random_num(num):
    import random
    random_in = []
    for i in range(1, num+1):
        random_in.append(i)
    random_out = []
    while len(random_in) > 0:
        temp = random.randint(1, num)
        if temp in random_in:
            random_in.remove(temp)
            random_out.append(temp)
    return random_out

if __name__ == '__mane__':
    print('RANDOM_NUM.PY is being ru directly')
else:
    print('RANDOM_NUM.PY has being imported')