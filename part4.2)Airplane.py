"""
Создайте новый класс Airplane. Создайте следующие характеристики (полей)
объекAта:
● make (марка)
● model
● year
● max_speed
● odometer
● is_flying
и методы имитирующих поведение самолета take off (взлет), fly (летать), land
приземление). Метод take off должен изменить is_flying на True, а land на False. По
умолчанию поле is_flying = False. Метод fly должен принимать расстояние полета и
изменять показания одометра (километраж). Создайте 1 объект класса и используйте
все методы класса.
"""
class Airplane():


    def __init__(self, make, model, year, odometr, is_flying = False, max_speed = 900):
        self.make = make
        self.model = model
        self.year = year
        self.odometr = odometr
        self.is_flying = is_flying

    def take_off(self):
        self.is_flying = True
        print('Airplane {} {} takes off'.format(self.make, self.model))

    def fly_(self, distance = 500):
        self.odometr += distance
        print('Current odometr {}mil'.format(self.odometr))

    def land(self):
        self.is_flying = False
        print('Airplane land\'s')


flight102 = Airplane('Boing', '747', 1987, 1000000)
flight102.take_off()
flight102.fly_(1000)
flight102.land()





