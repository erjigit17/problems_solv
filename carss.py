# Правило LEGB
# Local локальная область функции
# Enclosing область внешней функции
# Global глобальное пространство
# Built-in встроенном (модуль builtis)


#################
#
#
# x = 10
# print('Global x     =', x, id(x))
#
#
# def fun_enclosing():
#     print('Enclosing x  =', x, id(x))
#
#     def fun_local():
#
#         print('Local x      =', x, id(x))
#
#     fun_local()
#
# fun_enclosing()
#
# print('Global after x', x, id(x))

# ############### 1
# x = 10
# print('Global x          =', x, id(x))
#
#
# def fun_enclosing():
#     x = 20  # создания новой переменной в пространстве имен fun_enclosing
#     print('Enclosing x       =', x, id(x))
#
#     def fun_local():
#         print('Local x           =', x, id(x))
#
#     fun_local()
#     print('Enclosing after x =', x, id(x))
# fun_enclosing()
#
# print('Global after x    =', x, id(x))
#
# ################ 2
#
#
# x = 10
# print('Global x          =', x, id(x))
#
#
# def fun_enclosing():
#     global x # Если необходимо изменить глобальную переменную необходимо сначала указать ее с инструкцией global
#     x = 20
#     print('Enclosing x       =', x, id(x))
#
#     def fun_local():
#         print('Local x           =', x, id(x))
#
#     fun_local()
#     print('Enclosing after x =', x, id(x))
# fun_enclosing()
#
# print('Global after x    =', x, id(x))

# ############### 3
#
#
# x = 10
# print('Global x          =', x, id(x))
#
#
# def fun_enclosing():
#     x = 20
#     print('Enclosing x       =', x, id(x))
#
#     def fun_local():
#         global x # возврат глобальной переменной
#         print('Local x           =', x, id(x))
#
#     fun_local()
#     print('Enclosing after x =', x, id(x))
# fun_enclosing()
#
# print('Global after x    =', x, id(x))


############### 4
# x = 10
# print('Global x =', x)
#
#
# def fun_enclosing():
#     x = 20
#     print('Enclosing x =', x)
#
#     def fun_local():
#         nonlocal x
#         x = 30
#         print('Local x =', x)
#
#     fun_local()
#
#     print('Enclosing after x =', x)
#
# fun_enclosing()
#
# print('Global after x =', x)


################# Замыкание Python Closures

# def make_multiplier_of(n):
#     """принимает n и создает функцию(multiplier) которая будет умножать n на аргумент(x) из созданной функции"""
#
#     def multiplier(x):
#         """у объекта multiplier будет ссылка на переменную n,
#            к которой она сможет обращаться уже после создания"""
#         return x * n
#
#     return multiplier
#
# times3 = make_multiplier_of(3) # n = 3
# times5 = make_multiplier_of(5) # n = 5
#
# print(times3(9))
# print(make_multiplier_of(3)(9))
# print('')
# print(times5(3))
# print(times5(times3(2)), '\n\n')
#
#
#
# just_in_time = make_multiplier_of(1) # n = 1 ratio 1 : 1
# month_late = make_multiplier_of(1.2) # n = 1 ratio 1 : 1.2
#
# pay = 100
# print('Just in time {} $'. format(just_in_time(pay)))
# print('Month late {} $'. format(month_late(pay)))
#
#
#
# """Поскольку замыкания используются в качестве функций обратного вызова, они обеспечивают своего рода сокрытие данных.
# Это помогает нам сократить использование глобальных переменных.
# Когда в нашем коде мало функций, замыкания оказываются эффективным способом.
# Но если нам нужно иметь много функций, то перейдем к классу (ООП)."""

###########
#
# def func():
#     x = ['py', 3]
#
#     def inner():
#         print(x)
#         print('id: ', id(x))
#         print('')
#     return inner
#
# func()()
# ## dir(func()) вызывает список атрибутов
# d = dir(func())
# for i in range(len(d)):
#     print(d[i], end=' ')
#     if i % 5==0:
#         print()
#
# print('\n\n', func().__closure__, '\n')
#
# ## dir(func().__closure__[0]) вызывает список атрибутов
# dd = dir(func().__closure__[0])
# for i in range(len(dd)):
#     print(dd[i], end=' ')
#     if i % 5==0:
#         print()
#
# print('\n\n', func().__closure__[0].cell_contents, '\n',
#       'id: ', id(func().__closure__[0].cell_contents))

# #
