def gen_num(N: int, M: int, prefix = None):
    """генерирует все числа (с лидируещими незначишими нуля)
    в n - ричной системе счисления (n <= 10"""
    prefix = prefix or []
    if M == 0:
        print(prefix)
        return
    for digit in range(N):
        prefix.append(digit)
        gen_num(N, M-1, prefix)
        prefix.pop()

gen_num(10,2)