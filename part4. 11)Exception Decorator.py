def check_password(func):
    def wrapper(self, *args, **kwargs):
        # Do som checks
        if not self.password or not self.username:
            raise Exception("No password to change!")
        func(self, *args, **kwargs)
    return wrapper

def check_username(func):
    def wrapper(self, *args, **kwargs):
        # Do som checks
        if not self.password or not self.username:
            raise Exception("No username defined!")
        func(self, *args, **kwargs)
    return wrapper




class User(object):
    def __init__(self, username=None, password=None):
        self.username = username
        self.password = password

    @check_username
    def get_account_balance(self):
        pass

    @check_password
    def change_password(self):
        pass


u = User()
# u.get_account_balance()
u.change_password()