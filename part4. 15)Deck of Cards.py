import random

class Deck():
    activ = True

    def __init__(self):
        self.deck = []
        self.message = ''

    def generate_deck(self):
        suit = ['Hearts', 'Diamonds', 'Clubs', 'Spades']
        value = 'A,2,3,4,5,6,7,8,9,10,J,Q,K'.split(',')

        for i in range(len(suit)):
            for j in range(len(value)):
                self.deck.append(suit[i] + ' ' + str(value[j]))

    def mix_card(self):
        return random.shuffle(self.deck)

    def show_card(self):
        return self.deck[0]

    def del_card(self):
        self.message = self.deck.pop(0)

    def __str__(self):
        return "{}".format(self.message)

    def main(self):
        if Deck.activ:
            self.generate_deck()
            self.mix_card()
            Deck.activ = False
        self.show_card()
        self.__str__()
        self.del_card()

start = Deck()

for _ in range(51):
    bottom = input('Eny key to play or q to exit')
    if bottom == 'q':
        break
    start.main()
    print('                                ',start)







