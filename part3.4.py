# //Find minimal positive integer that is not in list. If list contains only negative numbers,
# return 1. All elements are integers:
# Example: [1,2,3,4,6] - Answer 5
# Example: [1,2,3] - Answer 4
# Example: [-1, -2, -6] - Answer 1
#
# Create effective algorithm
import time

start = time.time()

def find_minimal_positive_int(list):
    if max(list) < 0:
        return 1

    else:
        res = 2
        while True:
            if res not in list:
                return res
                break
            else:
                res += 1



list = [1,2,3,4,6]
# list = [int(x) for x in input('Enter list: ').split()]
print(find_minimal_positive_int(list))

end = time.time()
print(end - start)