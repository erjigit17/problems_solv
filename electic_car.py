from car import Car

class Battery():

    def __init__(self, battery_size=70):
        self.battery_size = battery_size

    def describe_battery(self):
        print('This car has a ' + str(self.battery_size) + 'kwh battery.')

    def get_range(self):
        if self.battery_size == 70:
            range = 240
        elif self.battery_size == 85:
            range = 270

        massege = 'this car can go approximately ' + str(range)
        massege += ' miles on a full charge.'
        print(massege)

    def upgrade_battery(self):
        if self.battery_size != 85:
            self.battery_size =85


class ElectricCar(Car):
    """aspects of electric cars"""

    def __init__(self, make, model, year):
        """initialisation aspects of mother class"""
        super().__init__(make, model, year)
        self.battery = Battery()

my_new_car = ElectricCar('Toyota', 'Prius', 2019)
print(my_new_car.get_descriptive_name())

my_new_car.battery.upgrade_battery()
my_new_car.battery.describe_battery()
my_new_car.battery.get_range()