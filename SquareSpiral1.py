
sides = eval(input("Ведите количество сторон от 2 до 6:"))
import turtle
t = turtle.Pen()
colors = ['lemon chiffon', 'red', 'blue', 'green', 'pink']
turtle.bgcolor('black')
for x in range(200):
    t.pencolor(colors[x%5])
    t.forward(x * 3/sides + x)
    t.left(360/sides - 1)
    t.width(x * sides/200)
    t.left(90)