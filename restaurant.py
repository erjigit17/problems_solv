class User():

    def __init__(self, first_name, last_name, age, sex):

        self.first_name = first_name
        self.last_name = last_name
        self.age = age
        self.sex = sex
        self.login_attempts = 0

    def describe_user(self):
        print(self.first_name)
        print(self.last_name)
        print(self.age)
        print(self.sex)
        print(self.login_attempts, ' login_attempts')

    def greet_user(self):
        print('Hi ', self.first_name, '!')

    def login_attempts(self, attempt):
        self.login_attempts += attempt


    def reset_login_attempts(self):
        self.login_attempts = 0


class Privileges():

    def __init__(self, privileges=[]):
        self.privileges = privileges


    def show_privileges(self):
        self.privileges = privileges
        print('Admin can: ')
        for privilege in privileges:
            print(privilege, end='\n')


class Admin(User):
    def __init__(self, first_name, last_name, age, sex):
        super().__init__(first_name, last_name, age, sex)
        self.privileges = Privileges()





privileges =['Del user', 'add user', 'block']
admin1 = Admin('erji', 'Imam', 31, 'male')
admin1.describe_user()
admin1.privileges.show_privileges()


# user1 = User('Erji', 'Imam', 31, 'male')
#
#
# user1.describe_user()
# user1.login_attempts(1)




