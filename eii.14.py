#Given a string that may or may not contain a letter of interest.
# Print the index location of the first and last appearance of f.
# If the letter f occurs only once, then output its index. If the letter f does not occur,
# then do not print anything.
# Don't use loops in this task.
text = input("Enter text:  ")
if text.find("f") != -1:
    print(text.find("f"))
elif text.find("f") != text.rfind("f"):
    print(text.rfind("f"))

