class MoneyFmt():

    def __init__(self, numb):
        self.numb = numb

    def update(self, new_numb):
        self.numb = new_numb
        return self.numb

    def total(self):
        total = self.hole_numb() + self.cents()
        return total

    def hole_numb(self):
        numb = str(int(abs(self.numb)))
        res = []
        i = len(numb)
        while i >= 3:
            res.append(numb[i - 3: i])
            i -= 3
        if len(numb) % 3 != 0:
            res.append(numb[0: len(numb) % 3])
        res.reverse()

        resolt = '$'
        resolt += '-' if self.numb < 0 else ''
        resolt += ','.join(res)
        return resolt

    def cents(self):
        cent = abs(self.numb - int(self.numb))
        cent = round(cent, 2)

        if cent != 0:
            cent = str(cent)[1:4]
        else:
            cent = ''
        return cent

    # def __repr__(self):
    #     return self.numb

    def __str__(self):
        return self.total()