def stepPerms(n):
    s = [1, 2, 4]
    if n < 3:
        return s[n-1]
    else:
        for i in range(n-3):
            s.append(s[i] +s[i+1] + s[i+2])
        return s[-1]



print(stepPerms(8))