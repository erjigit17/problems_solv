def myDecorator(func):
    def wrapper(a, b=1, *args, **kwargs):
        t = (a, b) + args
        print('Decorator**********')
        print("\targument a: ", a)
        print("\targument b: ", b)
        print("\targument args: ", args)
        print("\targument kwargs: ", kwargs)
        print('Decorator**********')
        print('Calling', func.__name__, '(' + str(t), str(kwargs) + ')')
        func(a, b, *args, **kwargs)
        print('Finished', func.__name__, '(' + str(a+b) + ')')
    return wrapper


@myDecorator
def testFunc(a, b=1, *args, **kwargs):
    print("argument a: ", a)
    print("argument b: ", b)
    print("argument args: ", args)
    print("argument kwargs: ", kwargs)

    return a + b

testFunc(2, 3, 4, 5, c=6, d=7)
print()
testFunc(2, c=5, d=6)
print()
testFunc(10)