# Попросить пользователя ввести текст. В результате вывести процент
# букв в верхнем регистре (заглавные) и в нижнем регистре (прописные).

def upper_cher(text):
    count_upper = 0
    count_lower = 0

    for c in text:
        if c.isupper():
            count_upper += 1
        elif c.islower():
            count_lower += 1

    total_char = count_upper + count_lower

    per_upp = round(100 * count_upper / total_char, 2)
    per_low = round(100 * count_lower / total_char, 2)

    print('{}% в верхнем и {}% в нижнем регистре.'.format(per_upp, per_low))

# text = input('Enter text: ')
text = 'nsdfhndnfsmnNKJH hkj asj sd nsf whjf fkf wfk wehwekjh dfJHK kjhKJh KJ   r j ' * 10 ** 2
upper_cher(text)
