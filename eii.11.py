text = input("Enter text:  ")
print("the third character \n" + text[2])
print("the second to last character \n" + text[-2:-1])
print("the first five characters \n" + text[0:5])
print("all but the last two characters \n" + text[:-2])
print("all the characters of this string with even indices \n" + text[0::2])
print("all the characters of this string with odd indices\n" + text[1::2])
print("all the characters of the string in reverse order\n" + text[::-1])
print("every second character of the string in reverse order, starting from the last one\n" + text[::-2])
print("lenth of the string\n" + str(len(text)))


