def choice_to_number(num):
    if num == 'Usain':
        return 1
    elif num == 'Me':
        return 2
    elif num == 'Aybek':
        return 3

def number_to_choice(choi):
    if choi == 1:
        return 'Usain'
    elif choi == 2:
        return 'Me'
    elif choi == 3:
        return 'Aybek'





# if number is 1 then return usain bolt.
#DO NOT remove lines below here,
#this is designed to test your code.
def test_choice_to_number():
  assert choice_to_number('Usain') == 1
  assert choice_to_number('Me') == 2
  assert choice_to_number('Aybek') == 3

def test_number_to_choice():
  assert number_to_choice(1) == 'Usain'
  assert number_to_choice(2) == 'Me'
  assert number_to_choice(3) == 'Aybek'

def test_all():
  try:
    test_choice_to_number()
    test_number_to_choice()

  except AssertionError:
    print("Wrong")

  else:
    print("You are awsem")
#test your code by un-commenting the line(s) below
test_all()