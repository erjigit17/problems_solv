# Given a list of numbers, find and print all elements that are an even number.
# In this case use a for-loop that iterates over the list,
# and not over its indices! That is, don't use range()
# 1 3 4 5 77 87 98 103 111 118
n = input().split()
n_map = map(int, n)
for i in n_map:
    if (i % 2) == 0:
      print(i)
