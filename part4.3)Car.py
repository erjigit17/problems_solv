"""
Создайте класс Car. Пропишите в конструкторе параметры make, model, year,
odometer, fuel. Пусть у показателя odometer будет первоначальное значение 0,
а у fuel 70. Добавьте метод drive, который будет принимать расстояние в км. В
самом начале проверьте, хватит ли вам бензина из расчета того, что машина
расходует 10 л / 100 км (1л - 10 км) . Если его хватит на введенное расстояние,
то пусть этот метод добавляет эти километры к значению одометра, но не
напрямую, а с помощью приватного метода __add_distance. Помимо этого
пусть метод drive также отнимет потраченное количество бензина с помощью
приватного метода __subtract_fuel, затем пусть распечатает на экран “Let’s
drive!”. Если же бензина не хватит, то распечатайте “Need more fuel, please, fill
more!
"""

class Car():

    def __init__(self, make, model, year):
        self.make = make
        self.model = model
        self.year = year
        self.odometer = 0
        self.fuel = 70

    def drive(self, distance):
        print('{} {} {} set distance {}km'.format(self.make, self.model, self.year, distance))
        return self.__add_distance(distance)

    def __add_distance(self, distance, consumption_100 = 10):
        fuel_burn = distance / consumption_100
        if self.fuel - fuel_burn < 0:
            print('Need more fuel, please, fill more')
        else:
            self.odometer += distance
            print('Let’s drive!')
            print('Current odometr is {} km'.format(self.odometer))
            return self.__subtract_fuel(fuel_burn)

    def __subtract_fuel(self, fuel_burn):
        self.fuel -= fuel_burn
        print('You burn {}l, and you have {}l.'.format(fuel_burn, self.fuel))

# Car.__add_distance(fuel_burn)
# Car.__subtract_fuel(traveled)
# Car.__subtract_fuel(fuel_burn)

my = Car('Toyota', 'Prius', 2003)
my.drive(100)
my.drive(300)
my.drive(300)
my.drive(300)












